package org.eclipse.conesc.plugin.verifier;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NuSMVErrorParser {
	
	public static final String SYNTAX_ERROR = "syntax error";
	public static final String AMBIGUOUS = "ambiguous";
	public static final String DUPLICATE = "duplicate";
	public static final String MULTIPLE_DECLARATION = "multiple declaration";
	public static final String ABORT = "Aborting batch mode\n\nNuSMV terminated by a signal\n\nAborting batch mode";
	
	static public String analyze(String result, String model) {
		String error = "";
		for (String line:result.split("\\n")) {
			if (line.isEmpty() || line.startsWith("***")) continue;
			if (line.contains(SYNTAX_ERROR) || line.contains(AMBIGUOUS) || line.contains(DUPLICATE) || line.contains(MULTIPLE_DECLARATION)) {	
				// look for line
				Pattern p = Pattern.compile("(?<=line )\\d+");
		        Matcher m = p.matcher(line);
				if (!m.find()) continue;
				int l = Integer.valueOf(m.group());
				
				// look for token
				p = Pattern.compile("(?<=\")([^\"]*)");
		        m = p.matcher(line);
		        if (!m.find()) {
		        	// try with '
		        	p = Pattern.compile("(?<=\')([^\']*)");
			        m = p.matcher(line);
			        if (!m.find()) {
			        	// try with identifier:
			        	p = Pattern.compile("(?<=identifier:)(.*)");
				        m = p.matcher(line);
				        if (!m.find()) continue;
			        }
		        }
		        String token = m.group();
				
				String model_line = model.split("\\n")[l-1];
				
				// looking for type
				String type = "";
				if (model_line.contains("next_")) continue;
				if (model_line.contains("event")) type = "event";
				if (model_line.contains("_state")) type = "context";
				
				// looking for a faulty token
				String[] tokens = model_line.substring(model_line.indexOf("{")+1, model_line.indexOf("}")).split(",");
				String faulty_token = "";
				for (String t:tokens)
					if (t.contains(token)) {
						faulty_token = t.trim();
						break;
					}
				// if token is not found in neither contexts nor events, then it is a context group
				if (faulty_token.isEmpty()) {
					type = "context group";
					faulty_token = model_line.split("_")[0].trim();
				}
				
				error += "Error! ";
				if (line.contains(SYNTAX_ERROR)) error += "Syntax error at "+type;
				if (line.contains(AMBIGUOUS)) error += "Ambiguous";
				error += " name \""+faulty_token+"\".\n";
				if (line.contains(DUPLICATE)) error = "Error! Duplicate names at "+token.split("_")[0]+".\n";
				if (line.contains(MULTIPLE_DECLARATION)) error = "Error! Multiple declaration of "+token.split("_")[0]+".\n";
			}
		}
		// if our super parser did not find any error, it dows not mean there are none.
		// We display the error output even we did not find anything.
		if (error.isEmpty() && result.contains(ABORT)){
			for (String line : result.split("\\n")) {
				if (!line.isEmpty() && ABORT.contains(line)) break;
				if (line.isEmpty() || line.contains("***") || line.contains("WARNING")) continue;
				if (line.contains("line")) {
					Pattern p = Pattern.compile("(?<=line \\d{1,10}.{0,10}: ).*");
			        Matcher m = p.matcher(line);
			        if (!m.find()) continue;
			        error += m.group()+"\n";
			        continue;
				}
				error += line+"\n";
			}
			error = "Error!\n"+error;//.substring(0, error.lastIndexOf("\n"));
			//error = error.substring(0, error.lastIndexOf("\n"));
		}
		return error;
	}
	

}
