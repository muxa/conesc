package test;

import org.eclipse.conesc.plugin.verifier.NuSMVErrorParser;
import org.junit.Test;

import org.junit.Assert; 

public class TestNuSMVErrorParser {

	private String test_model_incorrect_event = ""
 + "MODULE main\n"
 + "VAR\n"
 + " CG1_state : {Ctx1, Ctx2};\n"
 + " event : {e, !e};\n"
 + " next_CG1_state : {Ctx1, Ctx2};\n"
 + "ASSIGN\n"
 + " next_CG1_state :=\n"
 + "  case\n"
 + "   CG1_state = Ctx1 & event = e : Ctx2;\n"
 + "   CG1_state = Ctx2 & event = !e : Ctx1;\n"
 + "   TRUE : CG1_state;\n"
 + "  esac;\n"
 + " next(CG1_state) :=\n"
 + "  case\n"
 + "   TRUE : next_CG1_state;\n"
 + "  esac;\n"
+ "SPEC AG (EF CG1_state = Ctx1)\n"
+ "SPEC AG (CG1_state = Ctx1 -> EF CG1_state = Ctx2)\n"
+ "SPEC AG (EF CG1_state = Ctx2)\n"
+ "SPEC AG (CG1_state = Ctx2 -> EF CG1_state = Ctx1)";
	
	private String test_output_incorrect_event = "*** This is NuSMV 2.6.0 (compiled on Wed Oct 14 15:32:58 2015)\n"
+ "*** Enabled addons are: compass\n"
+ "*** For more information on NuSMV see <http://nusmv.fbk.eu>\n"
+ "*** or email to <nusmv-users@list.fbk.eu>.\n"
+ "*** Please report bugs to <Please report bugs to <nusmv-users@fbk.eu>>\n"
+ "\n"
+ "*** Copyright (c) 2010-2014, Fondazione Bruno Kessler\n"
+ "\n"
+ "*** This version of NuSMV is linked to the CUDD library version 2.4.1\n"
+ "*** Copyright (c) 1995-2004, Regents of the University of Colorado\n"
+ "\n"
+ "*** This version of NuSMV is linked to the MiniSat SAT solver. \n"
+ "*** See http://minisat.se/MiniSat.html\n"
+ "*** Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson\n"
+ "*** Copyright (c) 2007-2010, Niklas Sorensson\n"
+ "\n"
+ "\n"
+ "file /Users/mike/eclipse/java-neon/Eclipse.app/Contents/MacOS/model.smv: line 4: at token \"!\": syntax error\n"
+ "\n"
+ "\n"
+ "file /Users/mike/eclipse/java-neon/Eclipse.app/Contents/MacOS/model.smv: line 4: Parser error\n"
+ "\n"
+ "NuSMV terminated by a signal\n"
+ "\n"
+ "Aborting batch mode\n"
+ "        0.06 real         0.00 user         0.01 sys";
	
	private String test_model_incorrect_context = ""
 + "MODULE main\n"
 + "VAR\n"
 + " CG1_state : {!Ctx1, Ctx2};\n"
 + " event : {e, not_e};\n"
 + " next_CG1_state : {Ctx1, Ctx2};\n"
 + "ASSIGN\n"
 + " next_CG1_state :=\n"
 + "  case\n"
 + "   CG1_state = Ctx1 & event = e : Ctx2;\n"
 + "   CG1_state = Ctx2 & event = !e : Ctx1;\n"
 + "   TRUE : CG1_state;\n"
 + "  esac;\n"
 + " next(CG1_state) :=\n"
 + "  case\n"
 + "   TRUE : next_CG1_state;\n"
 + "  esac;\n"
+ "SPEC AG (EF CG1_state = Ctx1)\n"
+ "SPEC AG (CG1_state = Ctx1 -> EF CG1_state = Ctx2)\n"
+ "SPEC AG (EF CG1_state = Ctx2)\n"
+ "SPEC AG (CG1_state = Ctx2 -> EF CG1_state = Ctx1)";
	
	private String test_output_incorrect_context = "*** This is NuSMV 2.6.0 (compiled on Wed Oct 14 15:32:58 2015)\n"
+ "*** Enabled addons are: compass\n"
+ "*** For more information on NuSMV see <http://nusmv.fbk.eu>\n"
+ "*** or email to <nusmv-users@list.fbk.eu>.\n"
+ "*** Please report bugs to <Please report bugs to <nusmv-users@fbk.eu>>\n"
+ "\n"
+ "*** Copyright (c) 2010-2014, Fondazione Bruno Kessler\n"
+ "\n"
+ "*** This version of NuSMV is linked to the CUDD library version 2.4.1\n"
+ "*** Copyright (c) 1995-2004, Regents of the University of Colorado\n"
+ "\n"
+ "*** This version of NuSMV is linked to the MiniSat SAT solver. \n"
+ "*** See http://minisat.se/MiniSat.html\n"
+ "*** Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson\n"
+ "*** Copyright (c) 2007-2010, Niklas Sorensson\n"
+ "\n"
+ "\n"
+ "file /Users/mike/eclipse/java-neon/Eclipse.app/Contents/MacOS/model.smv: line 3: at token \"!\": syntax error\n"
+ "\n"
+ "\n"
+ "file /Users/mike/eclipse/java-neon/Eclipse.app/Contents/MacOS/model.smv: line 3: Parser error\n"
+ "\n"
+ "NuSMV terminated by a signal\n"
+ "\n"
+ "Aborting batch mode\n"
+ "        0.06 real         0.00 user         0.01 sys";
	
	private String test_model_incorrect_context_g = ""
 + "MODULE main\n"
 + "VAR\n"
 + " !CG1_state : {Ctx1, Ctx2};\n"
 + " event : {e, not_e};\n"
 + " next_CG1_state : {Ctx1, Ctx2};\n"
 + "ASSIGN\n"
 + " next_CG1_state :=\n"
 + "  case\n"
 + "   CG1_state = Ctx1 & event = e : Ctx2;\n"
 + "   CG1_state = Ctx2 & event = !e : Ctx1;\n"
 + "   TRUE : CG1_state;\n"
 + "  esac;\n"
 + " next(CG1_state) :=\n"
 + "  case\n"
 + "   TRUE : next_CG1_state;\n"
 + "  esac;\n"
+ "SPEC AG (EF CG1_state = Ctx1)\n"
+ "SPEC AG (CG1_state = Ctx1 -> EF CG1_state = Ctx2)\n"
+ "SPEC AG (EF CG1_state = Ctx2)\n"
+ "SPEC AG (CG1_state = Ctx2 -> EF CG1_state = Ctx1)";
	
	private String test_output_incorrect_context_g = "*** This is NuSMV 2.6.0 (compiled on Wed Oct 14 15:32:58 2015)\n"
+ "*** Enabled addons are: compass\n"
+ "*** For more information on NuSMV see <http://nusmv.fbk.eu>\n"
+ "*** or email to <nusmv-users@list.fbk.eu>.\n"
+ "*** Please report bugs to <Please report bugs to <nusmv-users@fbk.eu>>\n"
+ "\n"
+ "*** Copyright (c) 2010-2014, Fondazione Bruno Kessler\n"
+ "\n"
+ "*** This version of NuSMV is linked to the CUDD library version 2.4.1\n"
+ "*** Copyright (c) 1995-2004, Regents of the University of Colorado\n"
+ "\n"
+ "*** This version of NuSMV is linked to the MiniSat SAT solver. \n"
+ "*** See http://minisat.se/MiniSat.html\n"
+ "*** Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson\n"
+ "*** Copyright (c) 2007-2010, Niklas Sorensson\n"
+ "\n"
+ "\n"
+ "file /Users/mike/eclipse/java-neon/Eclipse.app/Contents/MacOS/model.smv: line 3: at token \"!\": syntax error\n"
+ "\n"
+ "\n"
+ "file /Users/mike/eclipse/java-neon/Eclipse.app/Contents/MacOS/model.smv: line 3: Parser error\n"
+ "\n"
+ "NuSMV terminated by a signal\n"
+ "\n"
+ "Aborting batch mode\n"
+ "        0.06 real         0.00 user         0.01 sys";
	
	private String test_model_ambiguous_event = ""
 + "MODULE main\n"
 + "VAR\n"
 + " !CG1_state : {Ctx1, Ctx2};\n"
 + " event : {event};\n"
 + " next_CG1_state : {Ctx1, Ctx2};\n"
 + "ASSIGN\n"
 + " next_CG1_state :=\n"
 + "  case\n"
 + "   CG1_state = Ctx1 & event = e : Ctx2;\n"
 + "   CG1_state = Ctx2 & event = !e : Ctx1;\n"
 + "   TRUE : CG1_state;\n"
 + "  esac;\n"
 + " next(CG1_state) :=\n"
 + "  case\n"
 + "   TRUE : next_CG1_state;\n"
 + "  esac;\n"
+ "SPEC AG (EF CG1_state = Ctx1)\n"
+ "SPEC AG (CG1_state = Ctx1 -> EF CG1_state = Ctx2)\n"
+ "SPEC AG (EF CG1_state = Ctx2)\n"
+ "SPEC AG (CG1_state = Ctx2 -> EF CG1_state = Ctx1)";
	
	private String test_output_ambiguous_event = "*** This is NuSMV 2.6.0 (compiled on Wed Oct 14 15:32:58 2015)\n"
+ "*** Enabled addons are: compass\n"
+ "*** For more information on NuSMV see <http://nusmv.fbk.eu>\n"
+ "*** or email to <nusmv-users@list.fbk.eu>.\n"
+ "*** Please report bugs to <Please report bugs to <nusmv-users@fbk.eu>>\n"
+ "\n"
+ "*** Copyright (c) 2010-2014, Fondazione Bruno Kessler\n"
+ "\n"
+ "*** This version of NuSMV is linked to the CUDD library version 2.4.1\n"
+ "*** Copyright (c) 1995-2004, Regents of the University of Colorado\n"
+ "\n"
+ "*** This version of NuSMV is linked to the MiniSat SAT solver. \n"
+ "*** See http://minisat.se/MiniSat.html\n"
+ "*** Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson\n"
+ "*** Copyright (c) 2007-2010, Niklas Sorensson\n"
+ "\n"
+ "\n"
+ "file /Users/mike/eclipse/java-neon/Eclipse.app/Contents/MacOS/model.smv: line 4: \"event\" ambiguous\n"
+ "\n"
+ "\n"
+ "file /Users/mike/eclipse/java-neon/Eclipse.app/Contents/MacOS/model.smv: line 3: Parser error\n"
+ "\n"
+ "NuSMV terminated by a signal\n"
+ "\n"
+ "Aborting batch mode\n"
+ "        0.06 real         0.00 user         0.01 sys";
	
	@Test
	public void testParseErrorInEventName() {
		String error = NuSMVErrorParser.analyze(test_output_incorrect_event, test_model_incorrect_event);
		
		Assert.assertEquals("Error! Syntax error at event name \"!e\".\n", error);
	}
	
	@Test
	public void testParseErrorInContextName() {
		String error = NuSMVErrorParser.analyze(test_output_incorrect_context, test_model_incorrect_context);
		
		Assert.assertEquals("Error! Syntax error at context name \"!Ctx1\".\n", error);
	}
	
	@Test
	public void testParseErrorInContextGroupName() {
		String error = NuSMVErrorParser.analyze(test_output_incorrect_context_g, test_model_incorrect_context_g);
		
		Assert.assertEquals("Error! Syntax error at context group name \"!CG1\".\n", error);
	}
	
	@Test
	public void testParseErrorAmbiguousEvent() {
		String error = NuSMVErrorParser.analyze(test_output_ambiguous_event, test_model_ambiguous_event);
		
		Assert.assertEquals("Error! Ambiguous name \"event\".\n", error);
	}
}
