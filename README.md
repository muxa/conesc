# ConesC Project

We design and implement a complete tool-chain to support developers in designing, programming, and verifying adaptive WSN software using our approach.

The **GR**aphical **E**ditor and **VE**rifier for **C**ontext-**O**riented **M**odels (GREVECOM), which we design and implement as an Eclipse plug-in, allows designers to graphically compose the context-oriented design. Based on this, GREVECOM allows one to automatically generate ConesC templates later completed with application-specific functionality. The resulting implementations are handed over to a ConesC translator and then to the nesC compiler to produce a deployment-ready binary. Designers can also trigger a dedicated model generator that outputs a NuSMV model. This is input to NuSMV, along with predefined as well as developer-provided properties for running the actual verification. The NuSMV results are then parsed to express them using the same concepts of the initial context-oriented design.

## ConesC

ConesC brings concepts from Context-Oriented Programming (COP) down to WSN devices. Contexts model the situations that WSN software needs to adapt to. Using COP, programmers use a notion of layered function to implement context-dependent behavioral variations of WSN code. ConesC extends nesC with COP constructs. It greatly simplifies the resulting code and yields increasingly decoupled implementations compared to nesC. For example, there is a 50% reduction in the number of program states that programmers need to deal with, indicating easier debugging. In our tests, this comes at the price of a maximum 2.5% (4.5%) overhead in program (data) memory.

For more details please see [M. Afanasov et.al. "Software Adaptation in Wireless Sensor Networks." (To appear) ACM Transactions on Autonomous and Adaptive Systems](https://m-afanasov.com/publications/afanasov17software.pdf)

Here you can find the dedicated translator for ConesC written in Java. Please note, that you also need [TinyOS](http://www.tinyos.net/) and a nesC toolchain for the translator to work properly.

Useful links:

* [Getting Started](https://bitbucket.org/muxa/conesc/wiki/Getting%20Started)

* [Basics of ConesC](https://bitbucket.org/muxa/conesc/wiki/Basics-of-ConesC)

* [Advanced ConesC](https://bitbucket.org/muxa/conesc/wiki/Advanced-ConesC)

## Grevecom
GrEVeCOM is a Graphical Editor and Verifier for Context Oriented Models which allows you to draw your context-oriented model as a graphical diagram and generate a skeleton for ConesC application. Just try it!

Useful links:

* [Getting Started (temporarily unavailable)](https://bitbucket.org/neslabpolimi/conesc/wiki/Getting%20Started%20with%20Grevecom)